json.extract! post, :id, :titulo, :comentario, :data, :created_at, :updated_at
json.url post_url(post, format: :json)